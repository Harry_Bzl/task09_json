package analyzer;

import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;

class JSONSchemaValidator {
	
    static boolean validateJSON(String jsonSchemaDoc, String jsonDoc) throws JSONException,IOException {
    	
    	byte[] sxema = new byte[JSONSchemaValidator.class.getClassLoader().getResourceAsStream(jsonSchemaDoc).available()];
    	JSONSchemaValidator.class.getClassLoader().getResourceAsStream(jsonSchemaDoc).read(sxema);

    	byte[] doc = new byte[JSONSchemaValidator.class.getClassLoader().getResourceAsStream(jsonDoc).available()];
    	JSONSchemaValidator.class.getClassLoader().getResourceAsStream(jsonDoc).read(doc);

    	JSONObject jsonSubject = new JSONObject(new JSONTokener(new String(doc)));

    	JSONObject jsonSchema = new JSONObject(new JSONTokener(new String(sxema)));
    	Schema scheme = SchemaLoader.load(jsonSchema);
    	scheme.validate(jsonSubject);
    	return true;
    }

}
