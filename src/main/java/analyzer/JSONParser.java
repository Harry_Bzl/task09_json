package analyzer;

/**
 * class for parsing JSON file into Medicines class
 * @autor Igor
 * @version 1.1 from 01.11.18
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Medicines;
import java.io.File;
import java.io.IOException;

class JSONParser {
    private ObjectMapper objectMapper;

    JSONParser() {
        this.objectMapper = new ObjectMapper();
    }

    /**
     * parse JSON file.
     * @param jsonFile file for parsing
     * @return Medicines object from JSON file
     */
    Medicines parseMedicinesJSON(final File jsonFile) {
        Medicines medicines = new Medicines();
        try {
            medicines = objectMapper.readValue(jsonFile, Medicines.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return medicines;
    }
}
