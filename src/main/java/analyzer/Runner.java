package analyzer;

import model.Medicine;
import model.Medicines;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;

public class Runner {

    public static void main(String[] args) {
        File json = new File("src/main/resources/medicines.json");
        File schema = new File("src/main/resources/medicinesJSONScheme.json");

        try {
            if (JSONSchemaValidator.validateJSON(schema.getName(), json.getName()))
                System.out.println("Json file is validated successfully against Json Schema");
        }

        catch(JSONException | IOException je) {System.out.println(je.getMessage()+ "::ERROR: Correct your json file");
            return;}

        //validateJSON(json,schema);

        JSONParser parser = new JSONParser();

        printList(parser.parseMedicinesJSON(json));
    }

    private static void printList(Medicines medicines) {
        System.out.println("JSON");
        for (Medicine med : medicines.getMedicines()) {
            med.printMedicineInfo();
        }
    }


//    private static void validateJSON(File jsonDoc, File jsonScheme) {
//        /* / TODO: 04.11.2018 need analising, not worc clear */
//        try (InputStream inputStream = new FileInputStream(jsonScheme.getPath())) {
//            JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));
//            Schema schema = SchemaLoader.load(rawSchema);
//            schema.validate(new JSONObject(jsonDoc)); // throws a ValidationException if this object is invalid
//            System.out.println("Validation was successful!!!");
//        } catch (ValidationException e) {
//            System.out.println("No validated");
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }


}


