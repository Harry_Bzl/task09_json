package model;

/**
 * Class for describing Medicine
 * @autor Igor
 * @version 1.1 from 01.10.2018
 */

import java.util.ArrayList;
import java.util.List;

public class Medicine {
    private String name;
    private String pharm;
    private Group group;
    private List <String> analogs;
    private List <String> versions;
    private String certificate;
    private List<Integer> packages;
    private Dosage dosage;

    public Medicine (){
    }

    /**
     * constructor for Medicine object
     * @param name name of medicine
     * @param pharm manufacturer
     * @param group pharmacological group
     */
    public Medicine(String name, String pharm, Group group) {
        this.name = name;
        this.pharm = pharm;
        this.group = group;
        analogs = new ArrayList<String>();
        versions = new ArrayList<String>();
        packages = new ArrayList<Integer>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPharm(String pharm) {
        this.pharm = pharm;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public Dosage getDosage() {
        return dosage;
    }

    public void setDosage(Dosage dosage) {
        this.dosage = dosage;
    }

    public String getName() {
        return name;
    }

    public String getPharm() {
        return pharm;
    }

    public Group getGroup() {
        return group;
    }

    public List<String> getAnalogs() {
        return analogs;
    }

    public List<String> getVersions() {
        return versions;
    }

    public List<Integer> getPackages() {
        return packages;
    }

    public void addAnalog (String analog){
        analogs.add(analog);
    }

    public void addVersion (String version){
        versions.add(version);
    }

    public void addPackage (Integer pack){
        packages.add(pack);
    }

    public void setAnalogs(List<String> analogs) {
        this.analogs = analogs;
    }

    public void setVersions(List<String> versions) {
        this.versions = versions;
    }

    public void setPackages(List<Integer> packages) {
        this.packages = packages;
    }

    public void printMedicineInfo (){
        System.out.println("Name: "+ this.getName());
        System.out.println("Manufacturer: " + this.getPharm());
        System.out.println("Pharmacological group is: "+ this.getGroup().getName() + " with code: " + this.getGroup().getCode());
        System.out.print("Analogs are: ");
        for (String analog: analogs) System.out.print(analog + ", ");
        System.out.println();
        System.out.print("Versions are: ");
        for (String version: versions) System.out.print(version + ", ");
        System.out.println();
        System.out.println("Certificate: " + this.getCertificate());
        System.out.print("Packages are: ");
        for (Integer pack: packages) System.out.print(pack + ", ");
        System.out.println();
        System.out.println("Dosage: " + this.getDosage().getDose() + " " + this.getDosage().unit);
    }
}
