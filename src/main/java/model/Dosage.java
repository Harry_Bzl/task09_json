package model;

/**
 * class that describe dosage of medicine
 * @author Igor
 * @version 1.1 from 26.10.2018
 */

public class Dosage {
    String unit;
    Integer dose;

    public Dosage() {
    }

    /**
     * constructor for Dosage object
     * @param unit dosage unit (ex. g, mg)
     * @param dose dose of medicine
     */
    public Dosage(String unit, Integer dose) {
        this.unit = unit;
        this.dose = dose;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public Integer getDose() {
        return dose;
    }
}
