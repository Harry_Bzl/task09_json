package model;

/**
 * class that describe pharmacological group of medicine
 */
public class Group {
    String name;
    String code;

    public Group() {
    }

    /**
     * constructor for pharmacological group of medicine
     * @param name name of group
     * @param code code for ATC classification
     */
    public Group (String name, String code){
        this.name = name;
        this.code = code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }
}
