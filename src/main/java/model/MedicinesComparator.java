package model;

import java.util.Comparator;

public class MedicinesComparator implements Comparator<Medicine> {

  @Override
  public int compare(Medicine o1, Medicine o2) {
    return o1.getName().compareToIgnoreCase(o2.getName());
  }
}
